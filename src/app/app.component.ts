import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private metaService: Meta
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.metaService.addTags([
      { name: 'keywords', content: 'Kontra App, Entkraefter Pro Max, Joko & Klass, App, Thesen gegen Populisten, AFD' },
      { name: 'description', content: 'Gesellschaftliche Themen kompakt und fundiert erklärt' },
      { name: 'robots', content: 'index, follow' }
    ]);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
