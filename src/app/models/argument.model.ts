export interface Argument {
    topic: Topic;
    name: string;
    path: string;
    source: Array<Source>;
    text: string;
}

export enum Topic {
    Refugees,
    Climate,
    Racism,
    Covid,
    Economy
}

export class Source {
    constructor(public url: string, public date: Date = new Date(), public author: string = '') {
    }
}
