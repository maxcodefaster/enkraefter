import {InjectionToken} from '@angular/core';
import {Topic, Argument, Source} from "./argument.model";

export const PLAYLIST = new InjectionToken<string>('Playlist');

export const playlist: Argument[] = [
  {
    topic: Topic.Refugees,
    name: '1. Ausländer sind kriminell',
    path: './assets/mp3/kriminell.mp3',
    source: [
      new Source('https://www.bka.de/SharedDocs/Downloads/DE/Publikationen/PolizeilicheKriminalstatistik/2016/pks2016Jahrbuch3TV.pdf', new Date(), ''),
      new Source('https://www.zeit.de/news/2019-01/08/faktencheck-sind-auslaender-haeufiger-kriminell-als-deutsche-181221-99-316592', new Date(), ''),
      new Source('https://web.archive.org/web/20131202222025/http://www.berlin.de/imperia/md/content/lb-lkbgg/praevention/rechtsextremismus2/migration/gesemann.pdf', new Date(), ''),
      new Source('http://www.tagesspiegel.de/politik/migranten-besonders-kriminell-auslaenderkriminalitaet-ein-mythos/10269280.html', new Date(), ''),
    ],
    text: `Seit der erhöhten Zuwanderung im Jahr 2015, ist die Anzahl der angezeigten Gewalttaten gestiegen. Wie zum Beispiel Körperverletzungen
    und Sexualdelikte. Und Zuwandere tauchen laut Polizeistatistik überdurchschnittlich oftmals als Täter auf. Aber das liegt überwiegend an der
    Statistik selbst. Es werden sämtliche Tatverdächtigen gezählt und nicht lediglich die später vom Gericht verurteilten. In Deutschland werden
    allerdings Straftaten von Ausländern in der Regel doppelt so häufig angezeigt wie die von Deutschen. Der erhöhte Anteil von Fremden unter den
    Straftätern liegt genauso daran, dass in erster Regel junge Männer die Gefahr einer Flucht auf sich nehmen und zu uns kommen. Diese Gruppe
    begeht in jeder Gesellschaft die mehrheitlichen Straftaten. In deutschen Gefängnissen sind außerdem die meisten Insassen junge deutsche Männer.
    Trotzdem haben machen Personen das Gefühl, Flüchtlinge seien grundsätzlich krimineller. Dieser falsche Eindruck entsteht ebenfalls, weil
    Populisten schreckliche Einzelfälle instrumentalisieren. Nach dem Mord an einem Mädchen beispielsweise in dem Ort Kandel, demonstriert ein
    rechter Verein, seit zwei Jahren regelmäßig, weil der Täter aus Afghanistan stammt. Im gleichen Jahr wurden 713 Personen in Deutschland getötet.
    Nur 13 der Täter waren Flüchtlinge. Stammt jedoch ein Täter aus Deutschland, kommt es nahezu nie zu Demonstration. Komisch.`,
  },
  {
    topic: Topic.Refugees,
    name: '2. Seenotrettung fördert Schlepperei',
    path: './assets/mp3/seenotrettung.mp3',
    source: [
      new Source('https://www.law.ox.ac.uk/research-subject-groups/centre-criminology/centreborder-criminologies/blog/2017/03/border-deaths', new Date(), ''),
      new Source('https://sea-watch.org/das-projekt/faq/#1534414874883-c8957360-4b5b', new Date(), ''),
      new Source('https://www.zeit.de/2018/29/seenotrettung-fluechtlinge-privat-mittelmeer-pro-contra', new Date(), ''),
    ],
    text: `Seenotrettung fördert in erster Linie das Überleben der Menschen. Zehntausende wurden in den letzten Jahren vor dem Ertrinken gerettet.
    Skrupellose Schlepper hatten sie in Schlauchboote gesetzt, die in Seenot gerieten, sobald sie ablegten. Für tausend andere Flüchtlinge
    kam keine Hilfe. Das Mittelmeer ist mittlerweile ein Massengrab. Einige sagen, dass die freiwilligen Seenotretter die Schuld tragen und
    Komplizen der Schlepper sind. Sie sagen, sie müssen die Flüchtlinge daran hindern, in Boote einzusteigen, indem Sie ihnen die Hoffnung
    nehmen, in Europa anzukommen. Sie weisen auf den Rückgang der Flüchtlingszahlen seit der Schließung der italienischen Häfen hin. Diese
    Berechnung ist jedoch zweifelhaft. Die Zahl der Flüchtlinge war bereits rückläufig. Und als 2015 keine Rettungsschiffe unterwegs waren,
    ging die Zahl der ankommenden Flüchtlinge nicht zurück. Damals war nur ein Zusammenhang klar: Je weniger Seenotretter, desto mehr Tote.
    Inzwischen ist die absolute Zahl der Todesfälle gesunken, weil weniger Flüchtlingsboote unterwegs sind. Aber für diejenigen, die es
    trotzdem versuchen, ist die Gefahr des Ertrinkens größer als je zuvor. Nach Angaben der UN-Flüchtlingsbehörde stirbt jeder sechste Mensch.
    Die Gleichung von weniger Rettern, weniger Flüchtlingen ist nichts weiter als eine nicht unterstützte Behauptung. Womöglich ist es ein
    Fehlschluss, der jeden Tag Menschen sterben lässt.`,
  },
  {
    topic: Topic.Refugees,
    name: '3. Die meisten Flüchtlinge sind zuhause gar nicht in Gefahr',
    path: './assets/mp3/gefahr.mp3',
    source: [
      new Source('http://www.bpb.de/internationales/weltweit/innerstaatliche-konflikte/54705/syrien'),
      new Source('https://www.proasyl.de/thema/fakten-zahlen-argumente/')
    ],
    text: `Falsch, mehr als 60% aller Asylanträge im Jahr 2018 stammen von Menschen aus Syrien, dem Irak, Afghanistan, dem Iran, Nigeria,
    der Türkei oder Eritrea. Syrien, wo Präsident Assad seine Gegner mit Giftgas bekämpfte, und nach dem Ende des Bürgerkriegs viele
    seine Rache fürchten. Irak, in den die Deutschen nicht reisen sollten, weil nach dem Zitat des Auswärtigen Amtes "Landesweit mit schweren Angriffen
    und offenen bewaffneten Konflikten zu rechnen ist". Afghanistan, das Land, aus dem sich die Bundeswehr auch nach fast 20 Jahren nicht
    zurückziehen kann, weil die steinzeitlichen Islamisten des IS und der Taliban nach wie vor eine große Gefahr darstellen. Iran, wo es
    Auspeitschung, Folter und Amputationsstrafe gibt und eine der höchsten Hinrichtungszahlen der Welt. Nigeria, wo die Islamisten von
    Boko Haram Krieg gegen ihr eigenes Volk führen und man auf Landstraßen nicht ohne bewaffnete Eskorte fahren sollte. Die Türkei, wo ein
    falsches Like auf Facebook ins Gefängnis führen kann und zehntausende politische Gefangene inhaftiert sind. Eritrea, die Diktatur am Horn
    von Afrika. Wo Männer und Frauen oft jahrzehntelang Militärdienst leisten müssen, was der UN-Menschenrechtsrat Sklaverei nennt. Natürlich
    wird es in diesen Ländern auch Menschen geben, die ein erträgliches Leben führen. Aber ob sie es sind, die ihr Leben auf einem Schlauchboot
    im Mittelmeer riskieren ... Sehr unwahrscheinlich.`,
  },
  {
    topic: Topic.Refugees,
    name: '4. In Deutschland herrscht eine linke Meinungsdiktatur',
    path: './assets/mp3/meinungsdiktatur.mp3',
    source: [],
    text: `Ja wirklich? Seit fünf Jahren demonstriert Pegida an den besten Orten der Innenstädte. Jede Woche finden in Deutschland recht Aufmärsche
    statt. Die Demonstranten werden sogar von der Polizei beschützt, damit sie frei sprechen können. In allen großen Talkshows, von Anne-Will
    bis Markus Lanz, sind ständig rechte Politiker zu sehen. Die Bücher von rechten Autoren stehen auf der Bestsellerliste. Und allein in den
    letzten Jahren wurden mehr als 40 rechte Internetportale, Magazine, Radiosender und sogar ein Hipster-Magazin gegründet. In Deutschland gibt
    es wahrscheinlich jedes verrückte Thema in einer Zeitschrift. Karpfen, K-Pop oder Kiffen. Die Meinungsfreiheit deckt sogar den Verkauf ziemlich
    harter Satire, Beleidigungen und Verschwörungstheorien am Kiosk. Politisch ist jede Meinung von links nach rechts radikal vertreten. Nur freie
    Meinungsäußerung heißt nicht, dass einem niemand widersprechen kann. Man muss andere Meinungen in einer Demokratie ertragen. Aber viele Rechte
    können einfach nicht damit umgehen. Was sie eigentlich wollen ist, dass alle so reden und denken wie sie.`,
  },
  {
    topic: Topic.Refugees,
    name: '5. Um Flüchtlinge kümmert sich der Staat mehr als um “uns”',
    path: './assets/mp3/staat.mp3',
    source: [
      new Source('https://www.maz-online.de/Lokales/Havelland/Mehr-als-Hartz-IV-Was-Fluechtlinge-vom-Staat-bekommen'),
      new Source('https://www.bmas.de/DE/Presse/Pressemitteilungen/2019/reform-asylbewerberleistungsgesetz.html'),
      new Source('https://www.hartziv.org/regelbedarf.html')
    ],
    text: `Das Gegenteil ist wahr. Bedürftige Deutsche bekommen mehr  Geld als Flüchtlinge. Für Alleinstehende beträgt der Hartz-4-Standardsatz
    derzeit 424 Euro pro Monat. Alleinstehende Asylbewerber erhalten maximal 344 Euro. Also 80 Euro weniger. Asylsuchende in Erstaufnahmeeinrichtungen
    und Gemeinschaftsunterkünften erhalten hauptsächlich Sachleistungen wie Unterkunft, Verpflegung und medizinische Versorgung. Und nur ein
    Taschengeld von bis zu 150 Euro im Monat. Das Gesetz sieht jedoch genau wie bei bedürftigen Deutschen viele Kürzungen vor. Wenn Flüchtlinge
    anerkannt werden und bedürftig sind, haben sie genau den gleichen Anspruch auf Sozialleistungen wie deutsche Staatsbürger. Nicht mehr und nicht
    weniger. Laut der Flüchtlingshilfe der Vereinten Nationen ist es sehr unwahrscheinlich, dass die Menschen die erheblichen Risiken in Kauf nehmen,
    die mit einer Flucht verbunden sind, für eine solche Grundversorgung. Ja, viele Menschen in Deutschland sorgen sich auch um ihren Wohlstand.
    Das ist verständlich. Aber diejenigen, die diese Angst auf Flüchtlinge projizieren, treffen die falschen Leute. Der Schutz von Flüchtlingen ist auch
    eine völkerrechtliche Verpflichtung. Im vergangenen Jahr betrug der Bundeshaushalt übrigens 384 Milliarden Euro. Der Bund investierte 23 Milliarden
    Euro in sogenannte Flüchtlingsausgaben. Das ist sicherlich eine Menge, aber nur zum Vergleich: Dem Staat entgehen geschätzte 100 Milliarden Euro pro
    Jahr durch Steuerhinterziehung.`,
  },
  {
    topic: Topic.Refugees,
    name: '6. Das deutsche Volk soll systematisch ausgetauscht werden',
    path: './assets/mp3/volk.mp3',
    source: [
      new Source('https://www.cdu.de/faq-asyl'),
      new Source('https://www.zeit.de/politik/deutschland/2019-09/horst-seehofer-fluechtlingspolitik-malta-verteilung-bootsfluechtlinge'),
      new Source('https://www.boell.de/sites/default/files/geld_gegen_migration_-_der_nothilfe-treuhandfonds_fuer_afrika.pdf?dimension1=division_af'),
      new Source('https://ec.europa.eu/commission/presscorner/detail/de/MEMO_16_1616'),
      new Source('https://www.bmi.bund.de/SharedDocs/faqs/DE/themen/migration/fachkraefteeinwanderung/faqs-fachkraefteeinwanderungsgesetz.html'),
      new Source('https://www.welt.de/politik/deutschland/article182447686/Einwanderungsgesetz-CDU-und-CSU-wollen-nachschaerfen.html')
    ],
    text: `Wer das behauptet, ist von harten Rechtsextremisten getäuscht worden. Die Rede ist vom sogenannten großen Austausch der deutschen Bevölkerung
    oder einer angeblichen Umvolkung des Landes. Dies ist ein Verschwörungsmythos. Er zirkuliert seit Jahrzehnten in rechten Kreisen. Heute verbreitet
    ihn die AFD unter anderem. Solche Verschwörungsmythen verbreiten sich schnell, weil sie in einer verwirrenden Welt einfache Erklärungen und Sündenböcke
    bieten. Die Tatsache, dass Menschen weltweit fliehen, dass Flüchtlinge auch nach Europa kommen, hat wirklich sehr komplexe Gründe. Es hat mit Armut,
    Bürgerkriegen und Globalisierung zu tun. Und einfach nur dem Wunsch von Menschen, sich und ihren Kindern einen besseren Lebensraum zu bieten. Es ist
    absurd zu sagen, dass jemand Fluchtbewegungen auslösen oder lenken kann. Oder sicherstellen könne, dass sie ausgerechnet in Deutschland landen. Flüchtlinge
    und Migranten sind auf dem Weg, ob es uns gefällt oder nicht. Einige können in Deutschland ein neues Zuhause finden, was unserer alternden Gesellschaft
    übrigens gut tut. Die meisten fliehen jedoch woanders hin. In jedem Fall ist die deutsche Einwanderungspolitik inzwischen sehr streng. Dass sie das geheime
    Ziel hätte, mehr Ausländer hierher zu bringen, ist völliger Unsinn.`,
  },
  {
    topic: Topic.Refugees,
    name: '7. Linksextremisten sind mindestens so schlimm wie die Rechten',
    path: './assets/mp3/links.mp3',
    source: [
      new Source('https://www.bmi.bund.de/SharedDocs/downloads/DE/veroeffentlichungen/2019/pmk-2018.pdf?__blob=publicationFile&v=2', new Date(), '')
    ],
    text: `Das ist ein sehr alter rhetorischer Trick. Wenn man von etwas ablenken möchten, das einem unangenehm ist, verweist man auf ein anderes, anscheinend
    verwandtes Thema. Rauchen ist schädlich, aber auch Alkohol. Über das Wochenende nach London zu fliegen ist eine Klima-Sünde. Du musst mir nichts sagen,
    solange du Fleisch ist. Und so weiter und so fort. In ähnlicher Weise sollte der Hinweis auf linke Extreme von der Tatsache ablenken, dass die Rechten
    derzeit massiven Hass und Gewalt säen. Und dass sie für kein Problem eine vernünftige Lösung haben. Als Antwort genügen tatsächlich ein paar Sätze. Wie
    viele Menschen wurden seit der Wiedervereinigung von Tätern mit rechtem Hintergrund getötet? Mindestens 170. Und von links sind es ungefähr sieben. Auch
    die Polizeistatistik zur politischen Kriminalität ist hier eindeutig. Im Jahr 2018 wurden beispielsweise mehr als 20.000 Straftaten von rechts und weniger
    als 8.000 von links verzeichnet. Natürlich sind die Idioten auch auf der linken Seite. Aber der Unterschied zwischen den beiden Gruppen besteht, um es klar
    auszudrücken, darin, dass einige Autos anzünden, andere Menschen.`,
  },
  {
    topic: Topic.Refugees,
    name: '8. Deutschland kann nicht die ganze Welt retten',
    path: './assets/mp3/retten.mp3',
    source: [
      new Source('https://www.unhcr.org/5c52ea084.pdf', new Date(), ''),
      new Source('https://www.proasyl.de/thema/fakten-zahlen-argumente/', new Date(), ''),
    ],
    text: `Auch das macht Deutschland nicht. Die Zahl der Flüchtlinge weltweit ist mit 26 Millionen so hoch wie nie zuvor. Die Länder, die die meisten
    aufgenommen haben, sind nach Angaben der Vereinten Nationen die Türkei, Pakistan, Uganda und der Sudan. Nach Angaben des UNHCR lebten Ende 2018 eine
    Million anerkannte Flüchtlinge in Deutschland. In absoluten Zahlen ist Deutschland damit auf Platz fünf der Länder weltweit. Berücksichtigt man aber
    auch Dinge wie Bevölkerung oder Wirtschaftskraft, ändert sich das Bild erneut. Im Libanon, einem kleinen Nachbarland Syriens, ist beispielsweise jeder
    sechste ein Flüchtling. Wenn es in Deutschland genauso wäre, würden nicht eine Million Flüchtlinge in Deutschland leben, sondern etwa 14 Millionen.
    Der Sudan hingegen hat in absoluten Zahlen etwa so viele Flüchtlinge aufgenommen wie Deutschland, jedoch mit halb so vielen Einwohnern und einem weitaus
    geringeren Entwicklungsstand. Während die Zahl der Flüchtlinge weltweit weiter steigt, kommen zudem immer weniger Menschen nach Deutschland. 2016 wurden
    hier 745.000 Asylanträge gestellt. Zwei Jahre später waren es nur noch 186.000. Dies entspricht einem Rückgang von rund 75%.`,
  },
];
