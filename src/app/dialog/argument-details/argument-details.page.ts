import {Component, Input, OnInit} from '@angular/core';
import {Argument} from "../../models/argument.model";
import {ModalController} from "@ionic/angular";
import {DialogService} from "../dialog.service";

@Component({
  selector: 'app-argument-details',
  templateUrl: './argument-details.page.html',
  styleUrls: ['./argument-details.page.scss'],
})
export class ArgumentDetailsPage implements OnInit {
  @Input() argument: Argument;

  constructor(
    private modalCtrl: ModalController,
    public dialog: DialogService
  ) {
  }

  ngOnInit() {
  }

  async close() {
    await this.modalCtrl.dismiss();
  }

}
