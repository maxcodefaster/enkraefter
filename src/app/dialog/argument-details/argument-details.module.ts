import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArgumentDetailsPageRoutingModule } from './argument-details-routing.module';

import { ArgumentDetailsPage } from './argument-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArgumentDetailsPageRoutingModule
  ],
  declarations: [ArgumentDetailsPage]
})
export class ArgumentDetailsPageModule {}
