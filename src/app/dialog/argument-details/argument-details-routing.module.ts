import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArgumentDetailsPage } from './argument-details.page';

const routes: Routes = [
  {
    path: '',
    component: ArgumentDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArgumentDetailsPageRoutingModule {}
