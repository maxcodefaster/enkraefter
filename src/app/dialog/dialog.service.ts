import { Injectable } from '@angular/core';
import {AlertController} from "@ionic/angular";
import {Source} from "../models/argument.model";

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private alertCtrl: AlertController) { }

  async pwaAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Was ist eine PWA?',
      subHeader: 'Progressive Web Apps - Webseiten installierbar als App',
      message: `Steve Jobs präsentierte schon vor 12 jahren die Idee, dass Webseiten genau so ausschauen und sich verhalten sollten wie native Apps. Dies ist möglich mit
      Progressive Web Apps. Auf IPhones kann man sie mit Safari über den <i>Teilen</i> Knopf und anschließend einem Klick auf <i>Zum Startbildschirm hinzufügen.</i> Auf allen
      anderen Geräten von Android bis Windows ist das auf ähnliche Weise mit den gängigen Browsern möglich.`,
      buttons: [
        {
          text: 'Let me build your PWA',
          handler: () => {
            window.open('https://heichling.xyz', '_blank');
          }
        },
        {
          text: 'Ok',
          role: 'cancel',
        }
      ]
    });
    await alert.present();
  }

  async presentSources(sources: Source[]) {
    let sourcesReadable = '';
    for (const item of sources) {
      const match = item.url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
      sourcesReadable += '<a target="_blank" href="' + item + '">' + match[2] + '</a>' + '<br>';
    }
    const alert = await this.alertCtrl.create({
      header: 'Quellen',
      message: sourcesReadable,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
        }
      ]
    });

    await alert.present();
  }

  async comingSoonAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Coming soon!',
      message: `Bald wird die kontra.app auch Klimawandel-Leugner & Rassisten zur Besinnung bringen!`,
      buttons: [
        {
          text: 'heichling.xyz',
          handler: () => {
            window.open('https://heichling.xyz', '_blank');
          }
        },
        {
          text: 'Ok',
          role: 'cancel',
        }
      ]
    });

    await alert.present();
  }

  async presentAboutAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Über diese App & Impressum',
      message: `Die Idee zur kontra.app kam ursprünglich aus der Serie "Joko & Klass gegen ProSieben".
      Leider wurde daraus eine kleine Plastikbox, welche man für 10€ erwerben konnte. Wahrscheinlich hat man sich aus kommerziellen
      Interessen für diese Strategie entschieden, anstatt einen Entwickler einen Tag lang an die Entwicklung einer App dran zu setzen.
      <p><i>Deswegen habe ich mich dazu entschlossen genau dies zu machen.</i></p>
      <p>Impressum<br>Max Heichling<br>Glaspalast 1<br>86152 Augsburg</p>`,
      buttons: [
        {
          text: 'heichling.xyz',
          handler: () => {
            window.open('https://heichling.xyz', '_blank');
          }
        },
        {
          text: 'Ok',
          role: 'cancel',
        }
      ]
    });

    await alert.present();
  }
}
