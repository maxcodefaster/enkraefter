import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import {DialogService} from "../../dialog/dialog.service";

@Component({
  selector: 'app-home-popover',
  templateUrl: './about-popover.component.html',
  styleUrls: ['./about-popover.component.scss'],
})
export class AboutPopoverComponent {
  constructor(
    public popoverController: PopoverController,
    public dialog: DialogService,
  ) { }

  openAbout() {
    this.popoverController.dismiss();
    this.dialog.presentAboutAlert();
  }

  close() {
    this.popoverController.dismiss();
  }

}
