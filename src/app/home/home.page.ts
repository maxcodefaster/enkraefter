import {Component, ViewChild, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Howl} from 'howler';
import {PLAYLIST} from '../models/arguments';
import {Argument} from '../models/argument.model';
import {IonRange, IonContent, Platform, PopoverController, ModalController} from '@ionic/angular';
import {Plugins} from '@capacitor/core';
import {AboutPopoverComponent} from './about-popover/about-popover.component';
import {DialogService} from "../dialog/dialog.service";
import {ArgumentDetailsPage} from "../dialog/argument-details/argument-details.page";

const {Share} = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('range') range: IonRange;
  @ViewChild('ionContent') ionContent: IonContent;

  activeTrack: Argument = null;
  player: Howl = null;
  isStandalone = false;
  installTip = false;
  referral = false;
  platformIsNotIos = true;
  trackId = '';
  selectedSegment = 'refugee';

  isPlaying = false;
  progress = 0;
  songDuration = 0;
  seek = 0;
  bufferProgress = 10;
  showPlayer = false;
  playbackSpeed = 1;

  shareObject = {
    title: '',
    url: '',
    dialogTitle: 'kontra.app klärt auf: ',
    text: 'Diese These kann ich mit einer fundierten Antwort belegen.'
  };
  public webSocialShare: { show: boolean, share: any, onClosed: any } = {
    show: false,
    share: {
      displayNames: true,
      config: [{
        facebook: {
          socialShareText: '',
          socialShareUrl: '',
        },
      }, {
        twitter: {
          socialShareText: '',
          socialShareUrl: '',
        }
      }, {
        whatsapp: {
          socialShareText: '',
          socialShareUrl: '',
        }
      }]
    },
    onClosed: () => {
      this.webSocialShare.show = false;
    }
  };

  constructor(
    @Inject(PLAYLIST) public playlist: Argument[],
    private route: ActivatedRoute,
    private platform: Platform,
    private popoverCtrl: PopoverController,
    private modalCtrl: ModalController,
    public dialog: DialogService
  ) {
  }

  ngOnInit(): void {
    this.platformIsNotIos = !this.platform.is('ios');
    if (window.matchMedia('(display-mode: standalone)').matches) {
      this.isStandalone = true;
    }
    // check if track was referred
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.trackId = params.get('trackId');
    });
    if (this.trackId !== null) {
      this.referral = true;
      const trackInArray = +this.trackId;
      if (this.playlist[trackInArray]) {
        this.activeTrack = this.playlist[trackInArray];
        this.showPlayer = true;
        this.start(this.activeTrack, trackInArray);
      }
    }
  }

  ionViewDidEnter() {
    if (this.trackId) {
      this.scrollTo(this.trackId);
    }
  }

  segmentChanged(ev: any) {
    this.selectedSegment = ev.detail;
    this.dialog.comingSoonAlert();
  }

  async aboutPopover(event) {
    const popover = await this.popoverCtrl.create({
      component: AboutPopoverComponent,
      event
    });
    return await popover.present();
  }

  async showArgumentDetails(argument: Argument) {
    const modal = await this.modalCtrl.create({
      component: ArgumentDetailsPage,
      componentProps: {
        'argument': argument
      }
    });

    return await modal.present();
  }

  async shareTrack(title, trackId) {
    this.shareObject.url = location.protocol + '//' + window.location.hostname + '/home/' + trackId;
    this.shareObject.title = title;
    const navigator: any = window.navigator;
    if (navigator && navigator.share) {
      await Share.share({
        title,
        text: title + ': ' + this.shareObject.text,
        url: this.shareObject.url,
        dialogTitle: this.shareObject.dialogTitle,
      });
    } else {
      this.webSocialShare.share.config.forEach((item: any, key: string) => {
        if (item.whatsapp) {
          item.whatsapp.socialShareText = this.shareObject.title + ' 😠 ' + this.shareObject.text + ' 🤔 ' + this.shareObject.dialogTitle;
          item.whatsapp.socialShareUrl = this.shareObject.url;
        } else if (item.facebook) {
          item.facebook.socialShareText = this.shareObject.title + ' 😠 ' + this.shareObject.text + ' 🤔 ' + this.shareObject.dialogTitle;
          item.facebook.socialShareUrl = this.shareObject.url;
        } else if (item.twitter) {
          item.twitter.socialShareText = this.shareObject.title + ' 😠 ' + this.shareObject.text + ' 🤔 ' + this.shareObject.dialogTitle;
          item.twitter.socialShareUrl = this.shareObject.url;
        }
      });
      this.webSocialShare.show = true;
    }
  }

  start(track: Argument, i) {
    // return if track is already playing and show player if not visible
    if (this.activeTrack === track) {
      if (!this.showPlayer) {
        this.showPlayer = true;
      }
      return;
    }
    if (this.player) {
      this.player.stop();
    }
    this.activeTrack = track;
    this.showPlayer = true;
    this.trackId = i.toString();
    this.player = new Howl({
      src: [track.path],
      html5: true,
      preload: false,
      onplay: () => {
        this.isPlaying = true;
        this.updateProgress();
        this.playbackSpeed = 1;
      },
      onend: () => {
        // this.next();
      },
      onload: () => {
        this.songDuration = this.player.duration();
      }
    });
    this.player.play();
  }

  handleLoad() {
    const node: HTMLAudioElement = (this.player as any)._sounds[0]._node; // For Typescript
    const duration = this.player.duration();
    // https://developer.mozilla.org/en-US/Apps/Fundamentals/Audio_and_video_delivery/buffering_seeking_time_ranges
    if (duration > 0) {
      for (let i = 0; i < node.buffered.length; i++) {
        if (node.buffered.start(node.buffered.length - 1 - i) < node.currentTime) {
          this.bufferProgress = (node.buffered.end(node.buffered.length - 1 - i) / duration) * 100;
          // do what you will with it. I.E - store.set({ bufferProgress });
          break;
        }
      }
    }
  }

  togglePlayer(pause) {
    this.isPlaying = !pause;
    if (this.referral) {
      this.referral = false;
    }
    if (pause) {
      this.player.pause();
    } else {
      this.player.play();
    }

  }

  next() {
    const index = this.playlist.indexOf(this.activeTrack);
    if (index !== this.playlist.length - 1) {
      this.trackId = '' + (index + 1);
      this.start(this.playlist[this.trackId], this.trackId);
    } else {
      this.start(this.playlist[0], 0);
    }
  }

  prev() {
    const index = this.playlist.indexOf(this.activeTrack);
    if (index > 0) {
      this.trackId = '' + (index - 1);
      this.start(this.playlist[index - 1], index - 1);
    } else {
      this.start(this.playlist[this.playlist.length - 1], this.playlist.length - 1);
    }
  }

  changeSeek() {
    const newValue = +this.range.value;
    const duration = this.player.duration();
    this.player.seek(duration * (newValue / 100));
  }

  updateProgress() {
    this.handleLoad();
    if (isNaN(this.player.seek())) {
      this.seek = 0;
      this.progress = 0;
    } else {
      this.seek = this.player.seek();
      this.progress = (this.seek / this.player.duration()) * 100 || 0;
    }
    setTimeout(() => {
      this.updateProgress();
    }, 1000);
  }

  scrollTo(element: string) {
    const yOffset = document.getElementById(element).offsetTop;
    this.ionContent.scrollToPoint(0, yOffset, 4000);
  }

  toggleSpeed() {
    if (this.playbackSpeed < 2) {
      this.playbackSpeed += 0.25;
      this.player.rate(this.playbackSpeed);
    } else {
      this.playbackSpeed = 0.5;
      this.player.rate(this.playbackSpeed);
    }
  }

}
