import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MinuteSecondsPipe } from './minute-seconds.pipe';

import { HomePage } from './home.page';
import { PLAYLIST, playlist } from '../models/arguments';
import { AboutPopoverComponent } from './about-popover/about-popover.component';
import {EqualizerIconComponent} from "../components/equalizer-icon/equalizer-icon.component";
import {SkeletonComponent} from "../components/skeleton/skeleton.component";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [
    HomePage,
    AboutPopoverComponent,
    MinuteSecondsPipe,
    EqualizerIconComponent,
    SkeletonComponent
  ],
  entryComponents: [
    AboutPopoverComponent
  ],
  providers: [
    {
      provide: PLAYLIST, useValue: playlist
    }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class HomePageModule { }
